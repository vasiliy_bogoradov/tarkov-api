import { deflate } from "zlib";

export  function compress(data: object): Promise<Buffer> {
    return new Promise((res, rej) => {
        deflate(
            JSON.stringify(data),
            (err, result) => {
                if (err) {
                    rej(err);
                } else {
                    res(result);
                }
            }
        )
    });
}