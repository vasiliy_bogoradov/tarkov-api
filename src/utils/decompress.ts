import { inflate } from "zlib";

export function decompress(buffer: Buffer) : Promise<any> {
    return new Promise((res, reject) => {
        inflate(
            buffer,
            (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    res(JSON.parse(result.toString()));
                }
            }
        )
    });
}
