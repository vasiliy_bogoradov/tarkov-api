import {
    randomBytes,
} from 'crypto';

import {
    hashString
} from './hashString';

export function getRandomHash(short: boolean = false): string {
    const seed = randomBytes(20).toString('hex');
    const hash =  hashString(seed);
    if (short) {
        return hash.slice(0, -8)
    }
    return hash;
}