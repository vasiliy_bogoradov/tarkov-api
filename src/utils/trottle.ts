import {random } from 'lodash';

const queue: Function[] = [];
let lastWork_: number = 0;

export async function trottle() {
    return new Promise(res => {
        queue.push(res)
    })
}

(function work() {
    if (!queue.length) return setTimeout(work, 1000);

    const waitTime = random(3000, 5000);

    if ((Date.now() - lastWork_) > 1000) {
        queue.shift()();
        lastWork_ = Date.now();
        setTimeout(work, waitTime);
    } else {
        setTimeout(work, (lastWork_ - waitTime) - Date.now());
    }
})();
