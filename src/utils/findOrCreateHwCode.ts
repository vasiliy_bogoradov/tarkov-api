import {
    set,
    get,
} from "../storage";

import {
    generateHwCode,
} from "./generateHwCode";

export async function findOrCreateHwCode(): Promise<string> {
    let hwCode = await get('hwCode');

    if (!hwCode) {
        hwCode = generateHwCode();
        await set('hwCode', hwCode);
    }

    return Promise.resolve(hwCode);
}
