export interface Offer {
    _id: string;
    requirementsCost: number;
    itemsCost: number;
    user: User;
    items: Item[]
}

export interface User {
    nickname: string
}

export interface Item {
    _tpl: string
}