import {
    ParsedArgs,
} from 'minimist';
import {
    get,
    set,
} from '../storage';
import {
    prompt
} from 'inquirer';
import {
    hashString,
    generateHwCode,
    trottle,
} from "../utils";
import {
    login,
} from "../launcher";
import {
    NewHardwareError,
} from '../errors';

import {
    hardwareCodeActivateRequest,
} from '../api/launcher';

async function getOrFindCreds() {
    let cred = await get('auth', {})

    if (!cred || !cred.email || !cred.password) {
        if (!cred || !cred.email || !cred.password) {

            const newCreds = await prompt([
                {
                    type: 'input',
                    name: 'email',
                    message: 'Email:',
                    default: cred && cred.email,
                    validate(input: string) {
                        return input ? true : 'Email required';
                    },
                },
                {
                    type: 'password',
                    name: 'password',
                    message: 'Password:',
                    default: cred && cred.password,
                    mask: true,
                    validate(input: string) {
                        return input ? true : 'Password required';
                    },
                }
            ]);
            cred.email = newCreds.email;
            if (cred.password !== newCreds.password) {
                cred.password = hashString(newCreds.password);
            }
            await set('auth', cred);
        }
    }
    return cred;
}

async function getOrFindHwCode() {
    let hwCode = await get('hwCode');

    if (!hwCode) {
        if (!hwCode) {
            hwCode = generateHwCode();
        }
        const newHwCode = await prompt({
            type: 'input',
            name: 'hwCode',
            message: 'Hardware Code:',
            default: hwCode,
            validate(input: string) {
                return input.match(/^#1-([\da-z]+:){2}([\da-z]+-){5}([\da-z]+)$/) ? true : "Invalid HW Code"
            }
        });

        hwCode = newHwCode.hwCode;

        await set('hwCode', hwCode);
    }
    return hwCode;

}

export default async function auth(argv: ParsedArgs) {
    const cred = await getOrFindCreds();
    const hwCode = await getOrFindHwCode();
    await trottle();

    try {
        await login(cred);
    } catch (err) {
        if (!(err instanceof NewHardwareError)) throw err;

        const hwCodeConfirm = await prompt({
            type: 'input',
            name: 'code',
            message: 'New Hardware code detected, confirmotation has been sent to your email:'
        });

        await hardwareCodeActivateRequest({
            email: cred.email,
            activateCode: hwCodeConfirm.code,
            hwCode: hwCode.code,
        });
    }
}